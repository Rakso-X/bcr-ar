﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ControllerMenu : MonoBehaviour {

    public GameObject btnStartAR;
    public GameObject btnStartGame;
    public GameObject ARCAMERA;
    public GameObject canvasMenu;
    public GameObject canvasAR;

    public GameObject btnMostrarNiveles;
    public GameObject btnOcultarNiveles;

    public GameObject panelNiveles;

    public GameObject btnNoticias;

    // Use this for initialization
    void Start () {
        //Ocultando el Panel de Instucciones
        //panelDialogInstrucciones.SetActive(false);
        //panelStartScreen.SetActive(true);
        //cameraAr.SetActive(false);
        Screen.orientation = ScreenOrientation.Portrait;
    }
	
	// Update is called once per frame
	void Update () {

        Screen.orientation = ScreenOrientation.Portrait;

    }



    public void pressBtnInstrucciones()
    {
        //panelDialogInstrucciones.SetActive(true);
    }

    public void closeDialogOverlay()
    {
        //panelDialogInstrucciones.SetActive(false);
    }

    public void startARAnimatione()
    {
        //cameraAr.SetActive(true);
        //panelStartScreen.SetActive(false);
    }

    public void closeARAnimation()
    {
        //cameraAr.SetActive(false);
        //panelStartScreen.SetActive(false);
    }

    public void changeSceneToAR()
    {
        //SceneManager.LoadScene("ARScene");
        ARCAMERA.SetActive(true);
        canvasAR.SetActive(true);
        canvasMenu.SetActive(false);
    }
    public void changeSceneToGame()
    {
        SceneManager.LoadScene("GameMonedasYBilletesScene");
    }
    public void changeSceneToGuide()
    {
        SceneManager.LoadScene("GuiaScene");
    }

    public void goToMitosScene()
    {
        SceneManager.LoadScene("MitosScene");
    }

    public void backtoMenu()
    {
        //SceneManager.LoadScene("ARScene");
        canvasMenu.SetActive(true);
        ARCAMERA.SetActive(false);
        canvasAR.SetActive(false);
    }

    public void mostrarNiveles()
    {
        panelNiveles.SetActive(true);
    }

    public void ocultarNiveles()
    {
        panelNiveles.SetActive(false);
    }

    public void irNoticias()
    {
        SceneManager.LoadScene("Notification");
    }

}
