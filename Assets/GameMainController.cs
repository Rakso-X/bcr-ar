﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameMainController : MonoBehaviour {

    public GameObject btnBillete;

    // Use this for initialization
    void Start () {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    // Update is called once per frame
    void Update () {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    public void back()
    {
        SceneManager.LoadScene("GameMonedasYBilletesScene");
    }

}
