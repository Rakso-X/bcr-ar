﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameMonedasYBilletesController : MonoBehaviour {

    public GameObject btnBillete;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void changeSceneToMainJuegoBillete()
    {
        SceneManager.LoadScene("GameMainBillete");
    }

    public void changeSceneToMainJuegoBillete2()
    {
        SceneManager.LoadScene("GameMainBillete2Controller");
    }

    public void back()
    {
        SceneManager.LoadScene("MenuApp"); 
    }

}
