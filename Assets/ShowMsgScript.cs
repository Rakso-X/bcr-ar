﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowMsgScript : MonoBehaviour {

    public GameObject CloudMsg;

    public void OnMouseDown()
    {
        if (CloudMsg.activeSelf)
        {
            CloudMsg.SetActive(false);
        }
        else
        {
            hideAllCloudBox();
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void hideAllCloudBox()
    {
        foreach (GameObject fooObj in GameObject.FindGameObjectsWithTag("cloudbox"))
        {
            fooObj.SetActive(false);
            /*
            if (fooObj.name == "bar")
            {
                //Do Something
            }*/
        }
        CloudMsg.SetActive(true);
    }
}
